package com.sheraz.apidemo.ui.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sheraz.apidemo.R;
import com.sheraz.apidemo.models.ProfileModel;
import com.sheraz.apidemo.models.ProfileResponse;
import com.sheraz.apidemo.models.UserSignInModel;
import com.sheraz.apidemo.models.UserSignInResponse;
import com.sheraz.apidemo.network.RetrofitClient;
import com.sheraz.apidemo.storage.UserData;
import com.sheraz.apidemo.ui.MainActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    EditText et_firstName, et_lastName, et_userName, et_email;
    Button btnUpdate;

    String userName, userEmail, authentication, firstName, lastName;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initView();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });

    }

    private void initView() {

        et_firstName = findViewById(R.id.et_first_name);
        et_lastName = findViewById(R.id.et_last_name);
        et_userName = findViewById(R.id.et_user_name);
        et_email = findViewById(R.id.et_email);
        btnUpdate = findViewById(R.id.btn_update_profile);

    }

    @Override
    protected void onStart() {
        super.onStart();

        UserSignInModel userSignInModel = UserData.getInstance(ProfileActivity.this).getUser();

        et_firstName.setText(userSignInModel.getFirstName());
        et_lastName.setText(userSignInModel.getLastName());

        id = userSignInModel.getId();
        authentication = userSignInModel.getAccessToken();

        et_userName.setText(userSignInModel.getUsername());
        et_email.setText(userSignInModel.getEmail());


    }

    private void editProfile() {

        userName = et_userName.getText().toString();
        userEmail = et_email.getText().toString();
        firstName = et_firstName.getText().toString();
        lastName = et_lastName.getText().toString();

        Call<ProfileResponse> call = RetrofitClient.getInstance()
                .getApi().editProfile(id, authentication, firstName, lastName, userName, userEmail);

        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()){

                    ProfileResponse profileResponse = response.body();

                    Toast.makeText(ProfileActivity.this, profileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
