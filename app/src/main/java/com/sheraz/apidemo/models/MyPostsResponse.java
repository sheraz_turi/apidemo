package com.sheraz.apidemo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MyPostsResponse {

    @SerializedName("feeds")
    @Expose
    private ArrayList<MyPostsModel> myPostsModelList = null;

    public ArrayList<MyPostsModel> getMyPostsModelList() {
        return myPostsModelList;
    }

    public void setMyPostsModelList(ArrayList<MyPostsModel> myPostsModelList) {
        this.myPostsModelList = myPostsModelList;
    }
}
