package com.sheraz.apidemo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.sheraz.apidemo.R;
import com.sheraz.apidemo.models.MyPostsModel;
import com.sheraz.apidemo.models.PostDeleteResponse;
import com.sheraz.apidemo.models.UserSignInModel;
import com.sheraz.apidemo.network.RetrofitClient;
import com.sheraz.apidemo.storage.UserData;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyPostsAdapter extends RecyclerView.Adapter<MyPostsAdapter.MyViewHolder> {

    List<MyPostsModel> modelClassList;
    Context mContext;

    public MyPostsAdapter(List<MyPostsModel> modelClassList, Context mContext) {
        this.modelClassList = modelClassList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_posts_layout,parent,false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final MyPostsModel myPostsModel = modelClassList.get(position);
        holder.tv_title.setText(myPostsModel.getTitle());
        holder.tv_desc.setText(myPostsModel.getDescription());
        holder.tv_time.setText(myPostsModel.getCreatedAt());
        holder.tv_date.setText(myPostsModel.getUpdatedAt());

        holder.btnDeletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserSignInModel userSignInModel = UserData.getInstance(mContext).getUser();

                int id = userSignInModel.getId();
                String authorizationToken = userSignInModel.getAccessToken();
                int post_id = myPostsModel.getId();

                Call<PostDeleteResponse> call = RetrofitClient
                        .getInstance().getApi().deletePost(id,authorizationToken,post_id);

                call.enqueue(new Callback<PostDeleteResponse>() {
                    @Override
                    public void onResponse(Call<PostDeleteResponse> call, Response<PostDeleteResponse> response) {

                        if (response.isSuccessful()){

                            PostDeleteResponse postDeleteResponse = response.body();
                            Toast.makeText(mContext, postDeleteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();

                        }

                    }

                    @Override
                    public void onFailure(Call<PostDeleteResponse> call, Throwable t) {

                    }
                });

                Toast.makeText(mContext, ""+post_id, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return modelClassList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView tv_title, tv_desc, tv_time, tv_date;
        RelativeLayout relativeLayout;
        ImageButton btnDeletePost;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_my_post_title);
            tv_desc = itemView.findViewById(R.id.tv_my_desc);
            tv_time = itemView.findViewById(R.id.tv_my_post_time);
            tv_date = itemView.findViewById(R.id.tv_my_date);
            btnDeletePost = itemView.findViewById(R.id.ibtn_delete_post);

        }
    }
}
