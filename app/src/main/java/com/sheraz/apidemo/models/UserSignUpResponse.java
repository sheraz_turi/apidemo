package com.sheraz.apidemo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSignUpResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userSignUpModel")
    @Expose
    private UserSignUpModel userSignUpModel;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserSignUpModel getUserSignUpModel() {
        return userSignUpModel;
    }

    public void setUserSignUpModel(UserSignUpModel userSignUpModel) {
        this.userSignUpModel = userSignUpModel;
    }
}
