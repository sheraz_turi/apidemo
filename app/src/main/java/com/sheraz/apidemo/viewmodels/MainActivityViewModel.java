package com.sheraz.apidemo.viewmodels;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.sheraz.apidemo.adapters.MyPostsAdapter;
import com.sheraz.apidemo.models.MyPostsModel;
import com.sheraz.apidemo.models.MyPostsResponse;
import com.sheraz.apidemo.models.UserSignInModel;
import com.sheraz.apidemo.network.RetrofitClient;
import com.sheraz.apidemo.storage.UserData;
import com.sheraz.apidemo.ui.MainActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityViewModel extends ViewModel {

    private ArrayList<MyPostsModel> myPostsModelList;
    private UserSignInModel userSignInModel;
    private Context mContext;

    public ArrayList<MyPostsModel> getMyPosts(){

        userSignInModel = UserData.getInstance(mContext).getUser();
        int id = userSignInModel.getId();
        String authorizationToken = userSignInModel.getAccessToken();

        Call<MyPostsResponse> call = RetrofitClient
                .getInstance().getApi().getMyPosts(id,authorizationToken);


        call.enqueue(new Callback<MyPostsResponse>() {
            @Override
            public void onResponse(Call<MyPostsResponse> call, Response<MyPostsResponse> response) {

                if(response.isSuccessful()){

                    myPostsModelList = response.body().getMyPostsModelList();
//                    myPostsAdapter = new MyPostsAdapter(myPostsModelList,MainActivity.this);
//                    recyclerView.setAdapter(myPostsAdapter);
//                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL,false));
//                    myPostsAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<MyPostsResponse> call, Throwable t) {

                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return myPostsModelList;
    }


}
