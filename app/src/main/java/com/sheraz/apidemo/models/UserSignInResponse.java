package com.sheraz.apidemo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSignInResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("user")
    @Expose
    private UserSignInModel userSignInModel;

    public UserSignInResponse(Boolean success, String message, UserSignInModel userSignInModel) {
        this.success = success;
        this.message = message;
        this.userSignInModel = userSignInModel;
    }

    public UserSignInResponse() {
    }

    public UserSignInModel getUserSignInModel() {
        return userSignInModel;
    }

    public void setUserSignInModel(UserSignInModel userSignInModel) {
        this.userSignInModel = userSignInModel;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
