package com.sheraz.apidemo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserSignInModel {

    @SerializedName("password_salt")
    @Expose
    private String passwordSalt;

    @SerializedName("password_hash")
    @Expose
    private String passwordHash;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("active")
    @Expose
    private Boolean active;

    @SerializedName("last_signed_at")
    @Expose
    private String lastSignedAt;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password_reset_token")
    @Expose
    private Object passwordResetToken;

    @SerializedName("password_reset_sent_at")
    @Expose
    private Object passwordResetSentAt;

    @SerializedName("status")
    @Expose
    private Object status;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("access_token")
    @Expose
    private String accessToken;

    @SerializedName("first_name")
    @Expose
    private String firstName;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("country")
    @Expose
    private Object country;

    @SerializedName("avatar_url")
    @Expose
    private Object avatarUrl;

    @SerializedName("city")
    @Expose
    private Object city;

    @SerializedName("provider")
    @Expose
    private Object provider;

    @SerializedName("provider_uid")
    @Expose
    private Object providerUid;

    @SerializedName("user_type")
    @Expose
    private String userType;

    @SerializedName("device_id")
    @Expose
    private Object deviceId;

    @SerializedName("last_updated_coin")
    @Expose
    private Object lastUpdatedCoin;

    @SerializedName("collected_coin")
    @Expose
    private Integer collectedCoin;

    @SerializedName("latitude")
    @Expose
    private Object latitude;

    @SerializedName("longitude")
    @Expose
    private Object longitude;

    public UserSignInModel(Integer id, String lastSignedAt, String username, String email, String firstName, String lastName, String createdAt, String accessToken) {
        this.id = id;
        this.lastSignedAt = lastSignedAt;
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.createdAt = createdAt;
        this.accessToken = accessToken;
    }

    public UserSignInModel() {

    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getLastSignedAt() {
        return lastSignedAt;
    }

    public void setLastSignedAt(String lastSignedAt) {
        this.lastSignedAt = lastSignedAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(Object passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public Object getPasswordResetSentAt() {
        return passwordResetSentAt;
    }

    public void setPasswordResetSentAt(Object passwordResetSentAt) {
        this.passwordResetSentAt = passwordResetSentAt;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Object getCountry() {
        return country;
    }

    public void setCountry(Object country) {
        this.country = country;
    }

    public Object getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(Object avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Object getProvider() {
        return provider;
    }

    public void setProvider(Object provider) {
        this.provider = provider;
    }

    public Object getProviderUid() {
        return providerUid;
    }

    public void setProviderUid(Object providerUid) {
        this.providerUid = providerUid;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Object getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Object deviceId) {
        this.deviceId = deviceId;
    }

    public Object getLastUpdatedCoin() {
        return lastUpdatedCoin;
    }

    public void setLastUpdatedCoin(Object lastUpdatedCoin) {
        this.lastUpdatedCoin = lastUpdatedCoin;
    }

    public Integer getCollectedCoin() {
        return collectedCoin;
    }

    public void setCollectedCoin(Integer collectedCoin) {
        this.collectedCoin = collectedCoin;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

}
