package com.sheraz.apidemo.storage;

import android.content.Context;
import android.content.SharedPreferences;

import com.sheraz.apidemo.models.PostModel;
import com.sheraz.apidemo.models.UserSignInModel;

public class UserData {

    private static UserData mInstance;
    private Context mContext;

    private String userName, userEmail, firstName, lastName, lastSignedAt, createdAt, authorization;
    private int id;

    public UserData(Context mContext) {
        this.mContext = mContext;
    }

    public static synchronized UserData getInstance(Context mContext){

        if (mInstance == null){
            mInstance = new UserData(mContext);
        }
        return mInstance;
    }

    public void saveUser(UserSignInModel userSignInModel){

        id = userSignInModel.getId();
        authorization = userSignInModel.getAccessToken();
        userName = userSignInModel.getUsername();
        userEmail = userSignInModel.getEmail();
        firstName = userSignInModel.getFirstName();
        lastName = userSignInModel.getLastName();

        lastSignedAt = userSignInModel.getLastSignedAt();
        createdAt = userSignInModel.getCreatedAt();

    }


    public UserSignInModel getUser(){

        UserSignInModel userSignInModel = new UserSignInModel(
                id,
                lastSignedAt,
                userName,
                userEmail,
                firstName,
                lastName,
                createdAt,
                authorization
        );

        return userSignInModel;

    }


}
