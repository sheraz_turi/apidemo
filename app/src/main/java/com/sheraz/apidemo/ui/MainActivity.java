package com.sheraz.apidemo.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sheraz.apidemo.models.MyPostsResponse;
import com.sheraz.apidemo.viewmodels.MainActivityViewModel;
import com.sheraz.apidemo.R;
import com.sheraz.apidemo.adapters.MyPostsAdapter;
import com.sheraz.apidemo.models.MyPostsModel;
import com.sheraz.apidemo.models.PostResponse;
import com.sheraz.apidemo.models.SignOutResponse;
import com.sheraz.apidemo.models.UserSignInModel;
import com.sheraz.apidemo.network.RetrofitClient;
import com.sheraz.apidemo.storage.UserData;
import com.sheraz.apidemo.ui.profile.ProfileActivity;
import com.sheraz.apidemo.ui.signin.SignInActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView tvName, tvCreatePost, tvDesc, tvEmail;
    Button btnLogout, btnCreatePost;
    EditText etTitle, etDesc;
    ImageButton btnDeletePost;
    UserSignInModel userSignInModel;
    private Toolbar toolbar;
    SwipeRefreshLayout refreshLayout;

    ArrayList<MyPostsModel> myPostsModelList = new ArrayList<>();
    MyPostsAdapter myPostsAdapter;
    RecyclerView recyclerView;

//    MainActivityViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        gettingUserData();


        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMyPosts();
                refreshLayout.setRefreshing(false);
            }
        });


        tvCreatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etTitle.setVisibility(View.VISIBLE);
                etDesc.setVisibility(View.VISIBLE);
                btnCreatePost.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                refreshLayout.setVisibility(View.GONE);


            }
        });

        btnCreatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPost();

                etTitle.setVisibility(View.GONE);
                etDesc.setVisibility(View.GONE);
                btnCreatePost.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                refreshLayout.setVisibility(View.VISIBLE);

                getMyPosts();

            }
        });

    }


    private void initViews() {

        btnCreatePost = findViewById(R.id.btn_create_post);
        tvName = findViewById(R.id.tv_uName);
        tvCreatePost = findViewById(R.id.tv_create_post);
        etTitle = findViewById(R.id.et_post_title);
        etDesc = findViewById(R.id.et_post_description);
        recyclerView = findViewById(R.id.rv_my_posts);
        refreshLayout = findViewById(R.id.pull_to_refresh);

        toolbar = findViewById(R.id.main_activity_toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onStart() {
        super.onStart();


        getMyPosts();

    }


    private void gettingUserData() {

        userSignInModel = UserData.getInstance(MainActivity.this).getUser();
        tvName.setText("Wellcome");
        getSupportActionBar().setTitle(userSignInModel.getUsername());
    }


    private void signOut() {

        userSignInModel = UserData.getInstance(MainActivity.this).getUser();
        int id = userSignInModel.getId();
        String authorizationToken = userSignInModel.getAccessToken();

        Call<SignOutResponse> call = RetrofitClient
                .getInstance().getApi().signOutUser(id, authorizationToken);

        call.enqueue(new Callback<SignOutResponse>() {
            @Override
            public void onResponse(Call<SignOutResponse> call, Response<SignOutResponse> response) {

                SignOutResponse signOutResponse = response.body();

                if (signOutResponse != null && signOutResponse.getSuccess()) {

                    Intent intent = new Intent(MainActivity.this, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                    Toast.makeText(MainActivity.this, signOutResponse.getMessage(), Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(MainActivity.this, signOutResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<SignOutResponse> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void createPost() {

        String title = etTitle.getText().toString();
        String desc = etDesc.getText().toString();

        if (title.isEmpty()) {
            etTitle.setError("Title name is required");
            etTitle.requestFocus();
            return;
        }

        userSignInModel = UserData.getInstance(MainActivity.this).getUser();
        int id = userSignInModel.getId();
        String authorizationToken = userSignInModel.getAccessToken();

        Call<PostResponse> call = RetrofitClient
                .getInstance().getApi().createPost(id, authorizationToken, title, desc);

        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {

                PostResponse postResponse = response.body();

                if (postResponse.getSuccess()) {

                    Toast.makeText(MainActivity.this, postResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {

            }
        });

    }


    private void getMyPosts(){

        userSignInModel = UserData.getInstance(MainActivity.this).getUser();
        int id = userSignInModel.getId();
        String authorizationToken = userSignInModel.getAccessToken();

        Call<MyPostsResponse> call = RetrofitClient
                .getInstance().getApi().getMyPosts(id,authorizationToken);


        call.enqueue(new Callback<MyPostsResponse>() {
            @Override
            public void onResponse(Call<MyPostsResponse> call, Response<MyPostsResponse> response) {

                if(response.isSuccessful()){

                    myPostsModelList = response.body().getMyPostsModelList();
                    myPostsAdapter = new MyPostsAdapter(myPostsModelList,MainActivity.this);
                    recyclerView.setAdapter(myPostsAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL,false));
                    myPostsAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<MyPostsResponse> call, Throwable t) {

                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {

            case R.id.m_edit_profile:
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
                return true;

            case R.id.m_logout:
                signOut();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }
}
