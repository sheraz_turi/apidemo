package com.sheraz.apidemo.ui.signup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sheraz.apidemo.R;
import com.sheraz.apidemo.models.UserSignUpResponse;
import com.sheraz.apidemo.network.RetrofitClient;
import com.sheraz.apidemo.ui.MainActivity;
import com.sheraz.apidemo.ui.signin.SignInActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    EditText et_userName, et_userEmail, et_userPassword;
    TextView tv_Login;
    Button btnSignUp;

    String userName, userEmail, userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initViews();

        tv_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSignUp();
            }
        });
    }

    private void initViews() {
        et_userName = findViewById(R.id.et_sign_up_name);
        et_userEmail = findViewById(R.id.et_sign_up_email);
        et_userPassword = findViewById(R.id.et_sign_up_password);
        tv_Login = findViewById(R.id.tv_login);
        btnSignUp = findViewById(R.id.btn_signup);
    }

    private void userSignUp() {

        userName = et_userName.getText().toString();
        userEmail = et_userEmail.getText().toString();
        userPassword = et_userPassword.getText().toString();

        if (userName.isEmpty()) {
            et_userName.setError("UserSignUpModel name is required");
            et_userName.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            et_userEmail.setError("Enter a valid email");
            et_userEmail.requestFocus();
            return;
        }

        if (userPassword.isEmpty()) {
            et_userPassword.setError("Password is required");
            et_userPassword.requestFocus();
            return;
        }

        Call<UserSignUpResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .createUser(userName, userEmail, userPassword);

        call.enqueue(new Callback<UserSignUpResponse>() {
            @Override
            public void onResponse(Call<UserSignUpResponse> call, Response<UserSignUpResponse> response) {

                UserSignUpResponse userSignUpResponse = response.body();

                if (userSignUpResponse.getSuccess()){

                    Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                    Toast.makeText(SignUpActivity.this, userSignUpResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }
                else {
                    Toast.makeText(SignUpActivity.this, userSignUpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserSignUpResponse> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


//        Call<ResponseBody> call = RetrofitClient
//                .getInstance()
//                .getApi()
//                .createUser(userName, userEmail, userPassword);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//
//                String strResponse = null;
//
//                try {
//                    strResponse = response.body().string();
////                    Toast.makeText(SignUpActivity.this, strResponse, Toast.LENGTH_SHORT).show();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                if (strResponse != null){
//                    try {
//                        JSONObject jsonObject = new JSONObject(strResponse);
//                        JSONObject jsonObject1 = jsonObject.getJSONObject("user");
//
//                        Toast.makeText(SignUpActivity.this, jsonObject1.getString("username"), Toast.LENGTH_SHORT).show();
//
//                        String str = String.valueOf(jsonObject.getJSONObject("username"));
//                       // Toast.makeText(SignUpActivity.this, str, Toast.LENGTH_SHORT).show();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
    }

}
