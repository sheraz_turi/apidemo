package com.sheraz.apidemo.ui.signin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sheraz.apidemo.R;
import com.sheraz.apidemo.models.UserSignInResponse;
import com.sheraz.apidemo.network.RetrofitClient;
import com.sheraz.apidemo.storage.UserData;
import com.sheraz.apidemo.ui.MainActivity;
import com.sheraz.apidemo.ui.signup.SignUpActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {

    EditText et_userName, et_userEmail, et_userPassword;
    TextView tv_CreateAccount;
    Button btn_signIn;
    String userEmail, userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        initViews();

        tv_CreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        btn_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSignIn();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

//        if (UserData.getInstance(this).isLoggedIn()){
//
//            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//
//        }

    }

    private void initViews() {
        et_userEmail = findViewById(R.id.et_sign_in_email);
        et_userPassword = findViewById(R.id.et_sign_in_password);
        tv_CreateAccount = findViewById(R.id.tv_create_account);
        btn_signIn = findViewById(R.id.btn_signin);
    }

    private void userSignIn() {

        userEmail = et_userEmail.getText().toString();
        userPassword = et_userPassword.getText().toString();

        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            et_userEmail.setError("Enter a valid email");
            et_userEmail.requestFocus();
            return;
        }

        if (userPassword.isEmpty()) {
            et_userPassword.setError("Password is required");
            et_userPassword.requestFocus();
            return;
        }

        Call<UserSignInResponse> call = RetrofitClient
                .getInstance().getApi().loginUser(userEmail, userPassword);

        call.enqueue(new Callback<UserSignInResponse>() {
            @Override
            public void onResponse(Call<UserSignInResponse> call, Response<UserSignInResponse> response) {

                UserSignInResponse userSignInResponse = response.body();

                if (userSignInResponse != null && userSignInResponse.getSuccess()) {

                    UserData.getInstance(SignInActivity.this).saveUser(userSignInResponse.getUserSignInModel());

                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                    Toast.makeText(SignInActivity.this, userSignInResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }
                else {

                    Toast.makeText(SignInActivity.this, "Please Create your account", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(SignInActivity.this, SignUpActivity.class);
                    startActivity(intent);

                }

            }

            @Override
            public void onFailure(Call<UserSignInResponse> call, Throwable t) {

            }
        });

    }
}
