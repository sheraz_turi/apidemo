package com.sheraz.apidemo.network;

import com.sheraz.apidemo.models.MyPostsResponse;
import com.sheraz.apidemo.models.PostDeleteResponse;
import com.sheraz.apidemo.models.PostResponse;
import com.sheraz.apidemo.models.ProfileResponse;
import com.sheraz.apidemo.models.SignOutResponse;
import com.sheraz.apidemo.models.UserSignInResponse;
import com.sheraz.apidemo.models.UserSignUpResponse;

import org.w3c.dom.Text;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface Api {

    @FormUrlEncoded
    @POST("user/signup")
    Call<UserSignUpResponse> createUser(
            @Field("username") String username,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("user/signin")
    Call<UserSignInResponse> loginUser(
            @Field("email") String email,
            @Field("password") String password
    );

    @DELETE("user/signout")
    Call<SignOutResponse> signOutUser(
            @Header("id") int id,
            @Header("Authorization") String authorization
    );

    @FormUrlEncoded
    @POST("post")
    Call<PostResponse> createPost(
            @Header("id") int id,
            @Header("Authorization") String authorization,
            @Field("title") String title,
            @Field("description") String description
    );


    @GET("post/my_posts")
    Call<MyPostsResponse> getMyPosts(
            @Header("id") int id,
            @Header("Authorization") String authorization
    );

    @GET("post/delete_post")
    Call<PostDeleteResponse> deletePost(
            @Header("id") int id,
            @Header("Authorization") String authorization,
            @Query("post_id") int post_id
    );

    @FormUrlEncoded
    @PUT("user/edit_profile")
    Call<ProfileResponse> editProfile(
            @Header("id") int id,
            @Header("Authorization") String authorization,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("username") String username,
            @Field("email") String email
    );


}
